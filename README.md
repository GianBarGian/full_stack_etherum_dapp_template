# Full_Stack_Etherum_Dapp_Template

This is a template for a dapp built on top of Ethereum ecosystem, using solidity and truffle for developing smart contracts, node.js and mongodb as a backend and Next.js/Redux.js as a frontend. 
It use Ethers as a web3 integration library

## Installation

- Clone the project
- Install [Truffle](https://trufflesuite.com/docs/truffle/quickstart/)
- Install and start a local test blockchain with [Ganache](https://trufflesuite.com/docs/ganache/quickstart/)
- Inside the smart contracts folder run `truffle migrate` in a teminal
- Install the [metamask](https://metamask.io/download/) extension in the browser
- Inside the backend folder run `npm install` and `npm start` in a teminal
- Inside the smart contracts folder run `npm install` and `npm run dev` in a teminal
- Go to `http://localhost:3000/` in your browser


