import { useEffect, useState } from "react";
import { ethers } from "ethers";
import { useSelector, useDispatch } from "react-redux";

import {
  setIsInstalled,
  setIsProvider,
  setIsConnected,
  connectMetamask,
} from "../store/slices/metamaskSlice";

const Metamask = (props) => {
  const dispatch = useDispatch();

  const [provider, setProvider] = useState({});
  const [message, setMessage] = useState("");

  const isConnected = useSelector((state) => state.metamask.isConnected);
  const isInstalled = useSelector((state) => state.metamask.isInstalled);

  useEffect(() => {
    checkMetamask();
  }, []);

  const checkMetamask = async () => {
    if (!window.ethereum) {
      dispatch(setIsInstalled(false));
      return;
    }
    dispatch(setIsInstalled(true));
    const prov = new ethers.providers.Web3Provider(window.ethereum);
    setProvider(prov);
    dispatch(setIsProvider(prov._isProvider));
    const accounts = await prov.listAccounts();
    const isConn = !!accounts.length;
    dispatch(setIsConnected(isConn));
    if (isConn) {
      dispatch(connectMetamask(accounts[0]));
    }
  };

  const connectMm = async () => {
    if (!isInstalled) {
      setMessage("Please install Metamask");
      return;
    }
    const accounts = await provider.send("eth_requestAccounts", []);
    dispatch(setIsConnected(true));
    dispatch(connectMetamask(accounts[0]));
  };

  if (!isConnected)
    return (
      <div>
        <button onClick={connectMm}>Connect Metamask</button>
        {message && <p>{message}</p>}
      </div>
    );
  return null;
};

export default Metamask;
