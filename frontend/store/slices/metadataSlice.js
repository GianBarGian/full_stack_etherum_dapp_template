import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  data: {}
}

export const metaDataSlice = createSlice({
  name: 'metadata',
  initialState,
  reducers: {
    addMetadata: (state, action) => {
      state.data = action.payload
    }
  }
})

export const { addMetadata } = metaDataSlice.actions

export default metaDataSlice.reducer